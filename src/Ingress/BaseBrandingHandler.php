<?php declare(strict_types=1);

namespace Fittinq\Symfony\Configurator\Channel\Ingress;

use Fittinq\Symfony\Configurator\Channel\Entity\Branding;
use Fittinq\Symfony\Configurator\Channel\Exception\UnknownEventTypeException;
use Fittinq\Symfony\Configurator\Channel\Repository\BrandingRepository;
use Fittinq\Symfony\RabbitMQ\Handler\Handler;
use Fittinq\Symfony\RabbitMQ\RabbitMQ\HeaderBag;
use stdClass;

class BaseBrandingHandler extends Handler
{
    private BrandingRepository $repository;

    public function __construct(BrandingRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handleMessage(HeaderBag $headers, stdClass|array $body, string $exchange, string $routingKey): void
    {
        if (!$this->eventTypeIsValid($body->eventType)) {
            throw new UnknownEventTypeException();
        }

        if (!$this->repository->findByName($body->name)) {
            $branding = new Branding();
            $branding->setName($body->name);
            $this->repository->save($branding, true);
        }
    }

    private function eventTypeIsValid($eventType): bool
    {
        return $eventType == 'upsert';
    }
}