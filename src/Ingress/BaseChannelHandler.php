<?php declare(strict_types=1);

namespace Fittinq\Symfony\Configurator\Channel\Ingress;

use Fittinq\Symfony\Configurator\Channel\Entity\Branding;
use Fittinq\Symfony\Configurator\Channel\Entity\Channel;
use Fittinq\Symfony\Configurator\Channel\Entity\Platform;
use Fittinq\Symfony\Configurator\Channel\Exception\UnknownBrandingException;
use Fittinq\Symfony\Configurator\Channel\Exception\UnknownEventTypeException;
use Fittinq\Symfony\Configurator\Channel\Exception\UnknownPlatformException;
use Fittinq\Symfony\Configurator\Channel\Repository\BrandingRepository;
use Fittinq\Symfony\Configurator\Channel\Repository\ChannelRepository;
use Fittinq\Symfony\Configurator\Channel\Repository\PlatformRepository;
use Doctrine\ORM\NonUniqueResultException;
use Fittinq\Symfony\RabbitMQ\Handler\Handler;
use Fittinq\Symfony\RabbitMQ\RabbitMQ\HeaderBag;
use stdClass;

class BaseChannelHandler extends Handler
{
    private PlatformRepository $platformRepository;
    private BrandingRepository $brandingRepository;
    private ChannelRepository $channelRepository;

    public function __construct(PlatformRepository $platformRepository, BrandingRepository $brandingRepository, ChannelRepository $repository)
    {
        $this->platformRepository = $platformRepository;
        $this->brandingRepository = $brandingRepository;
        $this->channelRepository = $repository;
    }

    public function handleMessage(HeaderBag $headers, stdClass|array $body, string $exchange, string $routingKey): void
    {
        if (!$this->eventTypeIsValid($body->eventType)) {
            throw new UnknownEventTypeException();
        }

        if (!$platform = $this->platformRepository->findByName($body->platform)) {
            throw new UnknownPlatformException();
        }

        if (!$branding = $this->brandingRepository->findByName($body->branding)) {
            throw new UnknownBrandingException();
        }

        $this->createChannel($platform, $branding, $body);
    }

    private function eventTypeIsValid($eventType): bool
    {
        return $eventType == 'upsert';
    }

    /**
     * @throws NonUniqueResultException
     */
    public function createChannel(Platform $platform, Branding $branding, stdClass $body): void
    {
        $channel = $this->channelRepository->findByPlatformAndName($platform->getName(), $body->name);

        if (!$channel) {
            $channel = new Channel();
            $channel->setName($body->name);
            $channel->setPlatform($platform);
        }

        $channel->setBranding($branding);
        $channel->setLocale($body->locale);
        $channel->setCurrency($body->currency);

        $this->channelRepository->save($channel, true);
    }
}