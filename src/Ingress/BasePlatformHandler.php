<?php declare(strict_types=1);

namespace Fittinq\Symfony\Configurator\Channel\Ingress;

use Fittinq\Symfony\Configurator\Channel\Entity\Platform;
use Fittinq\Symfony\Configurator\Channel\Exception\UnknownEventTypeException;
use Fittinq\Symfony\Configurator\Channel\Repository\PlatformRepository;
use Doctrine\ORM\NonUniqueResultException;
use Fittinq\Symfony\RabbitMQ\Handler\Handler;
use Fittinq\Symfony\RabbitMQ\RabbitMQ\HeaderBag;
use stdClass;

class BasePlatformHandler extends Handler
{
    private PlatformRepository $repository;

    public function __construct(PlatformRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function handleMessage(HeaderBag $headers, stdClass|array $body, string $exchange, string $routingKey): void
    {
        if (!$this->eventTypeIsValid($body->eventType)) {
            throw new UnknownEventTypeException();
        }

        if (!$platform = $this->repository->findByName($body->name)) {
            $platform = new Platform();
            $platform->setName($body->name);
        }

        $platform->setPlatformType($body->platformType);
        $this->repository->save($platform, true);

    }

    private function eventTypeIsValid($eventType): bool
    {
        return $eventType == 'upsert';
    }
}