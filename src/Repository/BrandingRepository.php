<?php

namespace Fittinq\Symfony\Configurator\Channel\Repository;

use Fittinq\Symfony\Configurator\Channel\Entity\Branding;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Branding>
 *
 * @method Branding|null find($id, $lockMode = null, $lockVersion = null)
 * @method Branding|null findOneBy(array $criteria, array $orderBy = null)
 * @method Branding[]    findAll()
 * @method Branding[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BrandingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Branding::class);
    }

    public function save(Branding $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Branding $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findByName(string $name): ?Branding
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('branding')
            ->from('Fittinq\Symfony\Configurator\Channel\Entity\Branding', 'branding')
            ->where('branding.name LIKE :name')
            ->setParameter('name', $name);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}
