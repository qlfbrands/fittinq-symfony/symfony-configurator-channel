<?php

namespace Fittinq\Symfony\Configurator\Channel\Repository;

use Fittinq\Symfony\Configurator\Channel\Entity\Channel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Channel>
 *
 * @method Channel|null find($id, $lockMode = null, $lockVersion = null)
 * @method Channel|null findOneBy(array $criteria, array $orderBy = null)
 * @method Channel[]    findAll()
 * @method Channel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChannelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Channel::class);
    }

    public function save(Channel $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Channel $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findByPlatformAndName(string $platform, string $name): ?Channel
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('channel')
            ->from('Fittinq\Symfony\Configurator\Channel\Entity\Channel', 'channel')
            ->join('channel.platform', 'platform')
            ->where('platform.name = :platformName')
            ->andWhere('channel.name LIKE :channelName')
            ->setParameter('platformName', $platform)
            ->setParameter('channelName', $name);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    /**
     * @return Channel[]
     */
    public function getRetailChannels(): array
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('channel')
            ->from('Fittinq\Symfony\Configurator\Channel\Entity\Channel', 'channel')
            ->join('channel.platform', 'platform')
            ->andWhere('platform.name = :platformName')
            ->andWhere('platform.platformType = :platformType')
            ->andWhere('channel.deliveryChannel IS NOT NULL')
            ->setParameter('platformName', 'pos')
            ->setParameter('platformType', 'retail');

        return $queryBuilder->getQuery()->getResult();
    }
}
