<?php declare(strict_types=1);

namespace Fittinq\Symfony\Configurator\Channel;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SymfonyConfiguratorChannelBundle extends Bundle
{
}
