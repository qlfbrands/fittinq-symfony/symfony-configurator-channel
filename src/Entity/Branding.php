<?php

namespace Fittinq\Symfony\Configurator\Channel\Entity;

use Doctrine\ORM\Mapping as ORM;
use Fittinq\Symfony\Configurator\Channel\Repository\BrandingRepository;

#[ORM\Entity(repositoryClass: BrandingRepository::class)]
#[ORM\Table(name: 'branding')]
class Branding
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    protected ?int $id = null;

    #[ORM\Column(length: 255)]
    protected ?string $name = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
