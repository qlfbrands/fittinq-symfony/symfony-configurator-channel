<?php declare(strict_types=1);

namespace Fittinq\Symfony\Configurator\Channel\Exception;

use RuntimeException;

class UnknownPlatformException extends RuntimeException
{

}