<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Configurator\Channel\Ingress\Channel;

use stdClass;
use Fittinq\Symfony\Configurator\Channel\Ingress\BaseChannelHandler;
use Test\Fittinq\Symfony\Configurator\Channel\Repository\BrandingRepositoryMock;
use Test\Fittinq\Symfony\Configurator\Channel\Repository\ChannelRepositoryMock;
use Test\Fittinq\Symfony\Configurator\Channel\Repository\PlatformRepositoryMock;

class Configuration
{
    private ChannelRepositoryMock $channelRepository;
    private BaseChannelHandler $channelHandler;
    private PlatformRepositoryMock $platformRepository;
    private BrandingRepositoryMock $brandingRepository;

    public function __construct()
    {
        $this->channelRepository = new ChannelRepositoryMock();
        $this->platformRepository = new PlatformRepositoryMock();
        $this->brandingRepository = new BrandingRepositoryMock();
        $this->channelHandler = new BaseChannelHandler($this->platformRepository, $this->brandingRepository, $this->channelRepository);
    }

    public function configure(): BaseChannelHandler
    {
        return $this->channelHandler;
    }

    public function getPlatformRepository(): PlatformRepositoryMock
    {
        return $this->platformRepository;
    }

    public function getBrandingRepository(): BrandingRepositoryMock
    {
        return $this->brandingRepository;
    }

    public function getChannelRepository(): ChannelRepositoryMock
    {
        return $this->channelRepository;
    }

    public function setUpChannelMessage(string $eventType, string $platform, string $branding, string $name): stdClass
    {
        $message = new stdClass();
        $message->eventType = $eventType;
        $message->platform = $platform;
        $message->branding = $branding;
        $message->locale = 'nl_NL';
        $message->currency = 'EUR';
        $message->name = $name;

        return $message;
    }
}

