<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Configurator\Channel\Ingress\Channel;

use Fittinq\Symfony\Configurator\Channel\Entity\Branding;
use Fittinq\Symfony\Configurator\Channel\Entity\Platform;
use Fittinq\Symfony\Configurator\Channel\Exception\UnknownBrandingException;
use Fittinq\Symfony\Configurator\Channel\Exception\UnknownEventTypeException;
use Fittinq\Symfony\Configurator\Channel\Exception\UnknownPlatformException;
use Fittinq\Symfony\Configurator\Channel\Ingress\BaseChannelHandler;
use Fittinq\Symfony\RabbitMQ\RabbitMQ\HeaderBag;
use PHPUnit\Framework\TestCase;
use Test\Fittinq\Symfony\Configurator\Channel\Repository\ChannelRepositoryMock;
use Throwable;

class ChannelTest extends TestCase
{
    private Configuration $configuration;
    private ChannelRepositoryMock $channelRepository;
    private BaseChannelHandler $channelHandler;

    protected function setUp(): void
    {
        parent::setUp();

        $this->configuration = new Configuration();
        $this->channelHandler = $this->configuration->configure();
        $platformRepository = $this->configuration->getPlatformRepository();
        $brandingRepository = $this->configuration->getBrandingRepository();
        $this->channelRepository = $this->configuration->getChannelRepository();

        $magento = new Platform();
        $magento->setName('magento');
        $platformRepository->save($magento, true);
        $lampandlight = new Branding();
        $lampandlight->setName('lampandlight');
        $brandingRepository->save($lampandlight, true);
    }

    /**
     * @throws Throwable
     */
    public function test_unknownEventTypeShouldNackMessage()
    {
        $message = $this->configuration->setUpChannelMessage('unknown', 'magento', 'lampandlight', 'lampenlicht.nl');

        $this->expectException(UnknownEventTypeException::class);

        $this->channelHandler->handleMessage(new HeaderBag([]), $message, 'cnf.channel', '');
    }

    /**
     * @throws Throwable
     */
    public function test_unknownEventTypeShouldNotSaveChannelToDatabase()
    {
        $message = $this->configuration->setUpChannelMessage('unknown', 'magento', 'lampandlight', 'lampenlicht.nl');

        try {
            $this->channelHandler->handleMessage(new HeaderBag([]), $message, 'cnf.channel', '');
        } catch (Throwable) {}

        $this->channelRepository->expectChannelNotToBeSaved($message->platform, $message->name);
    }

    /**
     * @throws Throwable
     */
    public function test_unknownPlatformShouldNackMessage()
    {
        $message = $this->configuration->setUpChannelMessage('upsert', 'unknown', 'lampandlight', 'lampenlicht.nl');

        $this->expectException(UnknownPlatformException::class);
        $this->channelHandler->handleMessage(new HeaderBag([]), $message, 'cnf.channel', '');

        $this->channelRepository->expectChannelNotToBeSaved($message->platform, $message->name);
    }

    /**
     * @throws Throwable
     */
    public function test_unknownPlatformShouldNotSaveChannelToDatabase()
    {
        $message = $this->configuration->setUpChannelMessage('upsert', 'unknown', 'lampandlight', 'lampenlicht.nl');

        try {
            $this->channelHandler->handleMessage(new HeaderBag([]), $message, 'cnf.channel', '');
        } catch (Throwable) {}

        $this->channelRepository->expectChannelNotToBeSaved($message->platform, $message->name);
    }

    /**
     * @throws Throwable
     */
    public function test_unknownBrandingShouldNackMessage()
    {
        $message = $this->configuration->setUpChannelMessage('upsert', 'magento', 'unknown', 'lampenlicht.nl');

        $this->expectException(UnknownBrandingException::class);
        $this->channelHandler->handleMessage(new HeaderBag([]), $message, 'cnf.channel', '');

        $this->channelRepository->expectChannelNotToBeSaved($message->platform, $message->name);
    }

    /**
     * @throws Throwable
     */
    public function test_unknownBrandingShouldNotSaveChannelToDatabase()
    {
        $message = $this->configuration->setUpChannelMessage('upsert', 'magento', 'unknown', 'lampenlicht.nl');

        try {
            $this->channelHandler->handleMessage(new HeaderBag([]), $message, 'cnf.channel', '');
        } catch (Throwable) {}

        $this->channelRepository->expectChannelNotToBeSaved($message->platform, $message->name);
    }

    /**
     * @throws Throwable
     */
    public function test_validEventMessageShouldSaveChannelToDatabase()
    {
        $message = $this->configuration->setUpChannelMessage('upsert', 'magento', 'lampandlight', 'lampenlicht.nl');

        $this->channelHandler->handleMessage(new HeaderBag([]), $message, 'cnf.channel', '');

        $this->channelRepository->expectChannelToBeSaved($message);
    }

    /**
     * @throws Throwable
     */
    public function test_receivingEventMessageMultipleTimesShouldNotResultInMultipleChannels()
    {
        $message = $this->configuration->setUpChannelMessage('upsert', 'magento', 'lampandlight', 'lampenlicht.nl');

        for ($i = 0; $i < 3; $i++) {
            $this->channelHandler->handleMessage(new HeaderBag([]), $message, 'cnf.channel', '');
        }

        $this->channelRepository->expectChannelToBeUnique($message->name);
    }
}