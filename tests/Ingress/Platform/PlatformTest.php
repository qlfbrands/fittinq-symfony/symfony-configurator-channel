<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Configurator\Channel\Ingress\Platform;

use Fittinq\Symfony\Configurator\Channel\Exception\UnknownEventTypeException;
use Fittinq\Symfony\Configurator\Channel\Ingress\BasePlatformHandler;
use Fittinq\Symfony\RabbitMQ\RabbitMQ\HeaderBag;
use PHPUnit\Framework\TestCase;
use Test\Fittinq\Symfony\Configurator\Channel\Repository\PlatformRepositoryMock;
use Throwable;

class PlatformTest extends TestCase
{
    private Configuration $configuration;
    private BasePlatformHandler $platformHandler;
    private PlatformRepositoryMock $platformRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->configuration = new Configuration();
        $this->platformHandler = $this->configuration->configure();
        $this->platformRepository = $this->configuration->getPlatformRepository();
    }

    /**
     * @throws Throwable
     */
    public function test_unknownEventTypeShouldNackMessage()
    {
        $message = $this->configuration->setUpPlatformMessage('unknown', 'magento', 'webshop');

        $this->expectException(UnknownEventTypeException::class);

        $this->platformHandler->handleMessage(new HeaderBag([]), $message, 'cnf.platform', '');
    }

    /**
     * @throws Throwable
     */
    public function test_unknownEventTypeShouldNotSavePlatformToDatabase()
    {
        $message = $this->configuration->setUpPlatformMessage('unknown', 'magento', 'webshop');

        try {
            $this->platformHandler->handleMessage(new HeaderBag([]), $message, 'cnf.platform', '');
        } catch (Throwable) {}

        $this->platformRepository->expectPlatformNotToBeSaved($message->name);
    }

    /**
     * @throws Throwable
     */
    public function test_validEventMessageShouldSavePlatformToDatabase()
    {
        $message = $this->configuration->setUpPlatformMessage('upsert', 'magento', 'webshop');

        $this->platformHandler->handleMessage(new HeaderBag([]), $message, 'cnf.platform', '');

        $this->platformRepository->expectPlatformToBeSaved($message);
    }

    /**
     * @throws Throwable
     */
    public function test_receivingEventMessageMultipleTimesShouldNotResultInMultiplePlatforms()
    {
        $message = $this->configuration->setUpPlatformMessage('upsert', 'magento', 'webshop');

        for ($i = 0; $i < 3; $i++) {
            $this->platformHandler->handleMessage(new HeaderBag([]), $message, 'cnf.platform', '');
        }

        $this->platformRepository->expectPlatformToBeUnique($message->name);
    }
}