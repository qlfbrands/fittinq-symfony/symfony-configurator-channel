<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Configurator\Channel\Ingress\Platform;

use Fittinq\Symfony\Configurator\Channel\Ingress\BasePlatformHandler;
use stdClass;
use Test\Fittinq\Symfony\Configurator\Channel\Repository\PlatformRepositoryMock;

class Configuration
{
    private PlatformRepositoryMock $platformRepository;
    private BasePlatformHandler $platformHandler;

    public function __construct()
    {
        $this->platformRepository = new PlatformRepositoryMock();
        $this->platformHandler = new BasePlatformHandler($this->platformRepository);
    }

    public function configure(): BasePlatformHandler
    {
        return $this->platformHandler;
    }

    public function getPlatformRepository(): PlatformRepositoryMock
    {
        return $this->platformRepository;
    }

    public function setUpPlatformMessage(string $eventType, string $name, string $platformType): stdClass
    {
        $message = new stdClass();
        $message->eventType = $eventType;
        $message->name = $name;
        $message->platformType = $platformType;

        return $message;
    }
}

