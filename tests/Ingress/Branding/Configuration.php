<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Configurator\Channel\Ingress\Branding;

use Fittinq\Symfony\Configurator\Channel\Ingress\BaseBrandingHandler;
use stdClass;
use Test\Fittinq\Symfony\Configurator\Channel\Repository\BrandingRepositoryMock;

class Configuration
{
    private BrandingRepositoryMock $brandingRepository;
    private BaseBrandingHandler $brandingHandler;

    public function __construct()
    {
        $this->brandingRepository = new BrandingRepositoryMock();
        $this->brandingHandler = new BaseBrandingHandler($this->brandingRepository);
    }

    public function configure(): BaseBrandingHandler
    {
        return $this->brandingHandler;
    }

    public function getBrandingRepository(): BrandingRepositoryMock
    {
        return $this->brandingRepository;
    }

    public function setUpBrandingMessage(string $eventType, string $name): stdClass
    {
        $message = new stdClass();
        $message->eventType = $eventType;
        $message->name = $name;

        return $message;
    }
}
