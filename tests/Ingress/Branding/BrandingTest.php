<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Configurator\Channel\Ingress\Branding;

use Fittinq\Symfony\Configurator\Channel\Exception\UnknownEventTypeException;
use Fittinq\Symfony\Configurator\Channel\Ingress\BaseBrandingHandler;
use Fittinq\Symfony\RabbitMQ\RabbitMQ\HeaderBag;
use PHPUnit\Framework\TestCase;
use Test\Fittinq\Symfony\Configurator\Channel\Repository\BrandingRepositoryMock;
use Throwable;

class BrandingTest extends TestCase
{
    private Configuration $configuration;
    private BaseBrandingHandler $brandingHandler;
    private BrandingRepositoryMock $brandingRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->configuration = new Configuration();
        $this->brandingHandler = $this->configuration->configure();
        $this->brandingRepository = $this->configuration->getBrandingRepository();
    }

    /**
     * @throws Throwable
     */
    public function test_unknownEventTypeShouldNackMessage()
    {
        $message = $this->configuration->setUpBrandingMessage('unknown', 'lampandlight');
        $this->expectException(UnknownEventTypeException::class);
        $this->brandingHandler->handleMessage(new HeaderBag([]), $message, 'cnf.branding', '');
    }

    /**
     * @throws Throwable
     */
    public function test_unknownEventTypeShouldNotSaveBrandingToDatabase()
    {
        $message = $this->configuration->setUpBrandingMessage('unknown', 'lampandlight');

        try {
            $this->brandingHandler->handleMessage(new HeaderBag([]), $message, 'cnf.branding', '');
        } catch (Throwable) {}

        $this->brandingRepository->expectBrandingNotToBeSaved($message->name);
    }

    /**
     * @throws Throwable
     */
    public function test_validEventMessageShouldSaveBrandingToDatabase()
    {
        $message = $this->configuration->setUpBrandingMessage('upsert', 'lampandlight');
        $this->brandingHandler->handleMessage(new HeaderBag([]), $message, 'cnf.branding', '');
        $this->brandingRepository->expectBrandingToBeSaved($message->name);
    }

    /**
     * @throws Throwable
     */
    public function test_receivingEventMessageMultipleTimesShouldNotResultInMultipleBrandings()
    {
        $message = $this->configuration->setUpBrandingMessage('upsert', 'lampandlight');

        for ($i = 0; $i < 3; $i++) {
            $this->brandingHandler->handleMessage(new HeaderBag([]), $message, 'cnf.branding', '');
        }

        $this->brandingRepository->expectBrandingToBeUnique($message->name);
    }
}