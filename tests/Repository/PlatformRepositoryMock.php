<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Configurator\Channel\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Fittinq\Symfony\Configurator\Channel\Entity\Platform;
use Fittinq\Symfony\Configurator\Channel\Repository\PlatformRepository;
use PHPUnit\Framework\Assert;

class PlatformRepositoryMock extends PlatformRepository
{
    private EntityManagerMock $entityManager;

    /** @noinspection PhpMissingParentConstructorInspection */
    public function __construct()
    {
        $this->entityManager = new EntityManagerMock();
    }

    protected function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    public function findByName(string $name): ?Platform
    {
        foreach ($this->entityManager->getFlushedEntities() as $platform) {
            if ($platform instanceof Platform) {
                if ($platform->getName() === $name) {
                    return $platform;
                }
            }
        }

        return null;
    }

    public function expectPlatformNotToBeSaved(string $name): void
    {
        foreach ($this->entityManager->getFlushedEntities() as $platform) {
            if ($platform instanceof Platform) {
                Assert::assertNotEquals($name, $platform->getName());
            }
        }

        Assert::assertTrue(true);
    }


    /**
     * @throws NonUniqueResultException
     */
    public function expectPlatformToBeSaved(\stdClass $message): void
    {
        $platform = $this->findByName($message->name);
        Assert::assertEquals($platform->getName(), $message->name);
        Assert::assertEquals($platform->getPlatformType(), $message->platformType);
    }

    public function expectPlatformToBeUnique(string $name): void
    {
        Assert::assertEquals(1, $this->countPlatformsByName($name));
    }

    public function countPlatformsByName(string $name): int
    {
        $numFound = 0;

        foreach ($this->entityManager->getFlushedEntities() as $platform) {
            if ($platform instanceof Platform) {
                if ($platform->getName() === $name) {
                    $numFound++;
                }
            }
        }

        return $numFound;
    }
}