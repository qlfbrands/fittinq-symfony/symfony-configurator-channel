<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Configurator\Channel\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Fittinq\Symfony\Configurator\Channel\Entity\Branding;
use Fittinq\Symfony\Configurator\Channel\Repository\BrandingRepository;
use PHPUnit\Framework\Assert;

class BrandingRepositoryMock extends BrandingRepository
{
    private EntityManagerMock $entityManager;

    /** @noinspection PhpMissingParentConstructorInspection */
    public function __construct()
    {
        $this->entityManager = new EntityManagerMock();
    }

    protected function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    public function findByName(string $name): ?Branding
    {
        foreach ($this->entityManager->getFlushedEntities() as $branding) {
            if ($branding instanceof Branding) {
                if ($branding->getName() === $name) {
                    return $branding;
                }
            }
        }

        return null;
    }

    public function expectBrandingNotToBeSaved(string $name): void
    {
        foreach ($this->entityManager->getFlushedEntities() as $branding) {
            if ($branding instanceof Branding) {
                Assert::assertNotEquals($name, $branding->getName());
            }
        }

        Assert::assertTrue(true);
    }


    public function expectBrandingToBeSaved(string $name): void
    {
        $this->expectBrandingToBeUnique($name);
    }

    public function expectBrandingToBeUnique(string $name): void
    {
        Assert::assertEquals(1, $this->countBrandingsByName($name));
    }

    public function countBrandingsByName(string $name): int
    {
        $numFound = 0;

        foreach ($this->entityManager->getFlushedEntities() as $branding) {
            if ($branding instanceof Branding) {
                if ($branding->getName() === $name) {
                    $numFound++;
                }
            }
        }

        return $numFound;
    }
}