<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Configurator\Channel\Repository;


use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Fittinq\Symfony\Configurator\Channel\Entity\Channel;
use Fittinq\Symfony\Configurator\Channel\Repository\ChannelRepository;
use PHPUnit\Framework\Assert;
use stdClass;

class ChannelRepositoryMock extends ChannelRepository
{
    private EntityManagerMock $entityManager;

    /** @noinspection PhpMissingParentConstructorInspection */
    public function __construct()
    {
        $this->entityManager = new EntityManagerMock();
    }

    protected function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    public function findByPlatformAndName(string $platform, string $name): ?Channel
    {
        foreach ($this->entityManager->getFlushedEntities() as $channel) {
            if ($channel instanceof Channel) {
                if ($channel->getPlatform()->getName() === $platform && $channel->getName() === $name) {
                    return $channel;
                }
            }
        }

        return null;
    }

    /**
     * @return Channel[]
     */
    public function getRetailChannels(): array
    {
        $channels = [];

        /** @var Channel $channel */
        foreach ($this->entityManager->getFlushedEntities() as $channel) {
            if ($channel->getPlatform()->getPlatformType() === 'retail') {
                $channels[] = $channel;
            }
        }

        return $channels;
    }

    public function expectChannelNotToBeSaved(string $platform, string $name): void
    {
        foreach ($this->entityManager->getFlushedEntities() as $channel) {
            if ($channel instanceof Channel) {
                Assert::assertFalse($platform === $channel->getPlatform()->getName() && $name ===  $channel->getName());
            }
        }

        Assert::assertTrue(true);
    }

    /**
     * @throws NonUniqueResultException
     */
    public function expectChannelToBeSaved(stdClass $message): void
    {
        $channel = $this->findByPlatformAndName($message->platform, $message->name);
        Assert::assertEquals($channel->getName(), $message->name);
        Assert::assertEquals($channel->getLocale(), $message->locale);
        Assert::assertEquals($channel->getCurrency(), $message->currency);
        Assert::assertEquals($channel->getBranding()->getName(), $message->branding);
    }

    public function expectChannelToBeUnique(string $name): void
    {
        Assert::assertEquals(1, $this->countChannelsByName($name));
    }

    public function countChannelsByName(string $name): int
    {
        $numFound = 0;

        foreach ($this->entityManager->getFlushedEntities() as $channel) {
            if ($channel instanceof Channel) {
                if ($channel->getName() === $name) {
                    $numFound++;
                }
            }
        }

        return $numFound;
    }
}