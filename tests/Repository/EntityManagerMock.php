<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Configurator\Channel\Repository;

use Doctrine\ORM\EntityManager;
use ReflectionException;
use ReflectionObject;

class EntityManagerMock extends EntityManager
{
    private array $persisted;
    private array $flushed;

    private static int $AUTO_INCREMENT = 0;

    /** @noinspection PhpMissingParentConstructorInspection */
    public function __construct()
    {
        $this->persisted = [];
        $this->flushed = [];
    }

    /**
     * @throws ReflectionException
     */
    public function persist($entity): void {
        if (!in_array($entity, $this->persisted)) {

            /* This hack is needed because since PHP 8.2 we can not dynamically declare variables, and we need to make
             * sure this object is unique by changing it's ID
             */
            $reflection = new ReflectionObject($entity);
            $idProperty = $reflection->getProperty('id');
            $idProperty->setAccessible(true);
            $idProperty->setValue($entity, self::$AUTO_INCREMENT++);

            $this->persisted[] = $entity;
        }
    }

    public function flush($entity = null): void
    {
        $this->flushed = $this->persisted;
    }

    public function getFlushedEntities(): array
    {
        return $this->flushed;
    }
}